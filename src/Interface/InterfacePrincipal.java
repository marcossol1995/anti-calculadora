package Interface;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;

import MetodosDatos.Datos;
import MetodosDatos.Metodos;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JLayeredPane;
import javax.swing.DropMode;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JProgressBar;
import java.awt.Color;

public class InterfacePrincipal {

	private JFrame frmAnticalculadora;
	private JLabel lblobjetivo;
	private JTextField textField;
	private JLabel lblNewLabel;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_1;
	private JTextField textField_5;
	private JTextField textField_6;
	private JLabel lblResultado;
	private JLabel lblNumero;
	private JLabel lblNumero_1;	
	private JFrame frame;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePrincipal window = new InterfacePrincipal();
					window.frmAnticalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfacePrincipal() {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("error setting native LAF: " + e);
		}
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		MetodosDatos.Datos juego = new MetodosDatos.Datos();
		Metodos.generarObjetivo(juego);
		frmAnticalculadora = new JFrame();
		frmAnticalculadora.getContentPane().setForeground(Color.BLACK);
		frmAnticalculadora.getContentPane().setBackground(Color.WHITE);
		frmAnticalculadora.setTitle("Anti-calculadora");
		frmAnticalculadora.setBounds(100, 100, 505, 483);
		frmAnticalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAnticalculadora.getContentPane().setLayout(null);

		lblobjetivo = new JLabel("Objetivo:");
		lblobjetivo.setBounds(23, 32, 65, 14);
		frmAnticalculadora.getContentPane().add(lblobjetivo);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(75, 29, 46, 20);
		frmAnticalculadora.getContentPane().add(textField);
		textField.setColumns(10);
		textField.setText(juego.getObjetivo().toString());

		lblNewLabel = new JLabel("Operadores restantes:");
		lblNewLabel.setBounds(23, 64, 145, 14);
		frmAnticalculadora.getContentPane().add(lblNewLabel);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(155, 61, 86, 20);
		frmAnticalculadora.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		Metodos.generarOperadores(juego);
		textField_2.setText(juego.getOperadoresRestantes().toString());

		JButton botonSum = new JButton("+");
		if(!juego.getOperadoresRestantes().contains("+")){
			botonSum.setEnabled(false);
		}
		botonSum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (MetodosDatos.Metodos.controlarNum(textField_1.getText()) && MetodosDatos.Metodos.controlarNum(textField_5.getText()) && Integer.parseInt(textField_5.getText())!=0) {
					if (juego.getResultado() == null) {
						MetodosDatos.Metodos.sumar(juego,Integer.parseInt(textField_1.getText()),Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					} else {
						MetodosDatos.Metodos.sumarAResultado(juego,Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					}
					textField_5.setText("");
					textField_1.setVisible(false);
					lblNumero.setVisible(false);
					textField_6.setLocation(50,235 );
					lblResultado.setLocation(50,220);
					Metodos.quitarOperador(juego, "+");
					textField_2.setText(juego.getOperadoresRestantes().toString());
					if(!(juego.getOperadoresRestantes().contains("+"))){
						botonSum.setEnabled(false);
					}
				} else {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								Interface.InterfaceError window = new Interface.InterfaceError();
								window.frmError.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}
		});
		botonSum.setBounds(193, 166, 89, 23);
		frmAnticalculadora.getContentPane().add(botonSum);

		JButton botonRes = new JButton("-");
		if(!juego.getOperadoresRestantes().contains("-")){
			botonRes.setEnabled(false);
		}
		botonRes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (MetodosDatos.Metodos.controlarNum(textField_1.getText()) && MetodosDatos.Metodos.controlarNum(textField_5.getText())&& Integer.parseInt(textField_5.getText())!=0) {
					if (juego.getResultado() == null) {
						MetodosDatos.Metodos.restar(juego,Integer.parseInt(textField_1.getText()),Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					} else {
						MetodosDatos.Metodos.restarAResultado(juego,Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					}
					textField_5.setText("");
					textField_1.setVisible(false);
					lblNumero.setVisible(false);
					textField_6.setLocation(50,235 );
					lblResultado.setLocation(50,220);
					Metodos.quitarOperador(juego, "-");
					textField_2.setText(juego.getOperadoresRestantes().toString());
					if(!juego.getOperadoresRestantes().contains("-")){
						botonRes.setEnabled(false);
					}
				} else {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								Interface.InterfaceError window = new Interface.InterfaceError();
								window.frmError.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}
		});
		botonRes.setBounds(193, 216, 89, 23);
		frmAnticalculadora.getContentPane().add(botonRes);

		JButton botonMult = new JButton("*");
		if(!juego.getOperadoresRestantes().contains("*")){
			botonMult.setEnabled(false);
		}
		botonMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (MetodosDatos.Metodos.controlarNum(textField_1.getText()) && MetodosDatos.Metodos.controlarNum(textField_5.getText()) && Integer.parseInt(textField_5.getText())!=1) {
					if (juego.getResultado() == null) {
						MetodosDatos.Metodos.multiplicar(juego,Integer.parseInt(textField_1.getText()),Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					} else {
						MetodosDatos.Metodos.multiplicarAResultado(juego,Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					}
					textField_5.setText("");
					textField_1.setVisible(false);
					lblNumero.setVisible(false);
					textField_6.setLocation(50,235 );
					lblResultado.setLocation(50,220);
					Metodos.quitarOperador(juego, "*");
					textField_2.setText(juego.getOperadoresRestantes().toString());
					if(!juego.getOperadoresRestantes().contains("*")){
						botonMult.setEnabled(false);
					}
				} else {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								Interface.InterfaceError window = new Interface.InterfaceError();
								window.frmError.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}
		});
		botonMult.setBounds(193, 264, 89, 23);
		frmAnticalculadora.getContentPane().add(botonMult);

		JButton botonDiv = new JButton("/");
		if(!juego.getOperadoresRestantes().contains("/")){
			botonDiv.setEnabled(false);
		}
		botonDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (MetodosDatos.Metodos.controlarNum(textField_1.getText()) && MetodosDatos.Metodos.controlarNum(textField_5.getText()) && Integer.parseInt(textField_5.getText())!=0 && Integer.parseInt(textField_5.getText())!=1) {
					if (juego.getResultado() == null) {
						MetodosDatos.Metodos.dividir(juego,Integer.parseInt(textField_1.getText()),Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					} else {
						MetodosDatos.Metodos.dividirAResultado(juego,Integer.parseInt(textField_5.getText()));
						textField_6.setText(juego.getResultado().toString());
					}
					textField_5.setText("");
					textField_1.setVisible(false);
					lblNumero.setVisible(false);
					textField_6.setLocation(50,235 );
					lblResultado.setLocation(50,220);
					Metodos.quitarOperador(juego, "/");
					textField_2.setText(juego.getOperadoresRestantes().toString());
					if(!juego.getOperadoresRestantes().contains("/")){
						botonDiv.setEnabled(false);
					}
					
				} else {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								Interface.InterfaceError window = new Interface.InterfaceError();
								window.frmError.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}
		});
		botonDiv.setBounds(193, 310, 89, 23);
		frmAnticalculadora.getContentPane().add(botonDiv);

		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(176, 413, 46, 20);
		frmAnticalculadora.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		textField_3.setText(juego.getAciertos().toString());

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(307, 413, 46, 20);
		frmAnticalculadora.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		textField_4.setText(juego.getFallos().toString());

		JLabel lblAciertos = new JLabel("Aciertos:");
		lblAciertos.setBounds(124, 416, 65, 14);
		frmAnticalculadora.getContentPane().add(lblAciertos);

		JLabel lblFallos = new JLabel("Fallos:");
		lblFallos.setBounds(268, 416, 46, 14);
		frmAnticalculadora.getContentPane().add(lblFallos);

		textField_1 = new JTextField();
		textField_1.setToolTipText("Solo se pueden ingresar n\u00FAmeros");
		textField_1.setBounds(50, 235, 86, 20);
		frmAnticalculadora.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		textField_5 = new JTextField();
		textField_5.setToolTipText("Solo se pueden ingresar numeros");
		textField_5.setBounds(334, 235, 86, 20);
		frmAnticalculadora.getContentPane().add(textField_5);
		textField_5.setColumns(10);

		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setBounds(50, 167, 86, 20);
		frmAnticalculadora.getContentPane().add(textField_6);
		textField_6.setColumns(10);

		lblResultado = new JLabel("Resultado:");
		lblResultado.setBounds(50, 154, 65, 14);
		frmAnticalculadora.getContentPane().add(lblResultado);

		lblNumero = new JLabel("Numero 1:");
		lblNumero.setBounds(50, 220, 63, 14);
		frmAnticalculadora.getContentPane().add(lblNumero);

		lblNumero_1 = new JLabel("Numero 2:");
		lblNumero_1.setBounds(335, 220, 58, 14);
		frmAnticalculadora.getContentPane().add(lblNumero_1);
		
		JButton btnSiguiente = new JButton("Siguiente");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(juego.getObjetivo().equals(juego.getResultado()) && juego.getOperadoresRestantes().isEmpty()){
					Metodos.aumentarAciertos(juego);
					if(juego.getAciertos()==10){
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									Victoria window = new Interface.Victoria();
									window.frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					}
				}else{
					Metodos.aumentarFallos(juego);
					if(juego.getFallos()==3){
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									Derrota window = new Interface.Derrota();
									window.frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					}
				}
				Metodos.generarObjetivo(juego);
				Metodos.generarOperadores(juego);
				juego.setResultado(null);
				textField_1.setText("");
				textField_5.setText("");
				textField_6.setText("");
				textField.setText(juego.getObjetivo().toString());
				textField_2.setText(juego.getOperadoresRestantes().toString());
				textField_3.setText(juego.getAciertos().toString());
				textField_4.setText(juego.getFallos().toString());
				textField_6.setLocation(50,168 );
				lblResultado.setLocation(50,154);
				textField_1.setVisible(true);
				lblNumero.setVisible(true);
				
				if(juego.getOperadoresRestantes().contains("+")){
					botonSum.setEnabled(true);
				}else{
					botonSum.setEnabled(false);
				}
				if(juego.getOperadoresRestantes().contains("-")){
					botonRes.setEnabled(true);
				}else{
					botonRes.setEnabled(false);
				}
				if(juego.getOperadoresRestantes().contains("*")){
					botonMult.setEnabled(true);
				}else{
					botonMult.setEnabled(false);
				}
				if(juego.getOperadoresRestantes().contains("/")){
					botonDiv.setEnabled(true);
				}else{
					botonDiv.setEnabled(false);
				}
			}
		});
		btnSiguiente.setBounds(331, 166, 89, 23);
		frmAnticalculadora.getContentPane().add(btnSiguiente);
		
		
	}
	
}
