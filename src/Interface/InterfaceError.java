package Interface;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.Window.Type;
import java.awt.Point;

public class InterfaceError {

	public JFrame frmError;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceError window = new InterfaceError();
					window.frmError.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfaceError() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmError = new JFrame();
		frmError.setType(Type.POPUP);
		frmError.setResizable(false);
		frmError.setTitle("Error!!!");
		frmError.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\marcos\\Pictures\\432.png"));
		frmError.getContentPane().setForeground(Color.RED);
		frmError.getContentPane().setBackground(Color.LIGHT_GRAY);
		frmError.setBounds(100, 100, 364, 202);
		frmError.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmError.getContentPane().setLayout(null);
		
		JTextPane txtpnAlgoSaliMal = new JTextPane();
		txtpnAlgoSaliMal.setEditable(false);
		txtpnAlgoSaliMal.setBounds(0, 0, 360, 172);
		txtpnAlgoSaliMal.setText("Algo sali\u00F3 mal!!! \r\nRevisa lo ingresado en las cajas de calculo.\r\n\r\nRecuerda:\r\n-No dejes ninguna caja de calculo en blanco\r\n-Solo puedes ingresar n\u00FAmeros\r\n-No se puede dividir por 0\r\n-No se pueden ingresar n\u00FAmeros mayores a 300\r\n-No pongas signos donde no corresponde\r\n-No puedes usar el elemento neutro de las operaciones");
		frmError.getContentPane().add(txtpnAlgoSaliMal);
	}
}
