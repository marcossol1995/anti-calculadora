package Interface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Derrota {

	JFrame frame;
	private JTextField txtTeEquivocaste;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Derrota window = new Derrota();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Derrota() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 270, 161);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtTeEquivocaste = new JTextField();
		txtTeEquivocaste.setText("Te equivocaste 3 veces, pierdes.");
		txtTeEquivocaste.setHorizontalAlignment(SwingConstants.CENTER);
		txtTeEquivocaste.setBounds(10, 11, 234, 100);
		frame.getContentPane().add(txtTeEquivocaste);
		txtTeEquivocaste.setColumns(10);
	}

}
