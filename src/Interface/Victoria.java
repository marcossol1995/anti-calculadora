package Interface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Victoria {

	JFrame frame;
	private JTextField txtLograsteAciertos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Victoria window = new Victoria();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Victoria() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 278, 154);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtLograsteAciertos = new JTextField();
		txtLograsteAciertos.setText("Lograste 10 aciertos, ganaste maquina!!!");
		txtLograsteAciertos.setHorizontalAlignment(SwingConstants.CENTER);
		txtLograsteAciertos.setBounds(10, 11, 242, 93);
		frame.getContentPane().add(txtLograsteAciertos);
		txtLograsteAciertos.setColumns(10);
	}

}
