package MetodosDatos;

import java.util.ArrayList;

public class Datos {
	private static Integer objetivo;
	private static Integer aciertos;
	private static Integer fallos;
	private static Integer resultado;
	private static ArrayList<String> operadoresRestantes;

	public Datos() {
		resultado = null;
		objetivo = 0;
		aciertos = 0;
		fallos = 0;
		operadoresRestantes = new ArrayList<String>();
	}

	public static ArrayList<String> getOperadoresRestantes() {
		return operadoresRestantes;
	}

	static void setOperadoresRestantes(ArrayList<String> nuevosOperadores) {
		operadoresRestantes=nuevosOperadores;
	}

	public static Integer getFallos() {
		return fallos;
	}

	static void setFallos(Integer v) {
		fallos += v;
	}

	public static Integer getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(Integer v) {
		this.objetivo = v;
	}

	public static Integer getAciertos() {
		return aciertos;
	}

	public void setAciertos(Integer v) {
		aciertos += v;
	}

	public static Integer getResultado() {
		return resultado;
	}

	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}

}
