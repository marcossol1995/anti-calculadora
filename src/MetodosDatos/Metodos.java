package MetodosDatos;

import java.util.ArrayList;
import java.util.Random;

public class Metodos {
	static Random generator = new Random();

	public static void sumar(Datos juego, Integer a, Integer b) {
		juego.setResultado(a + b);
	}

	public static void sumarAResultado(Datos juego, Integer b) {
		juego.setResultado(juego.getResultado() + b);
	}

	public static void restar(Datos juego, Integer a, Integer b) {
		juego.setResultado(a - b);
	}

	public static void restarAResultado(Datos juego, Integer b) {
		juego.setResultado(juego.getResultado() - b);
	}

	public static void multiplicar(Datos juego, Integer a, Integer b) {
		juego.setResultado(a * b);
	}

	public static void multiplicarAResultado(Datos juego, Integer b) {
		juego.setResultado(juego.getResultado() * b);
	}

	public static void dividir(Datos juego, Integer a, Integer b) {
		juego.setResultado(a / b);
	}

	public static void dividirAResultado(Datos juego, Integer b) {
		juego.setResultado(juego.getResultado() / b);
	}

	public static boolean controlarNum(String text) {
		if (text.equals(null) || text.equals(""))
			return false;
		if(contarSignosMenos(text)>=2)
			return false;
		if(contarSignosMenos(text.substring(1,text.length()))>0)
			return false;
		if(text.equals("-"))
			return false;
		if (text.equals(null) || text.equals(""))
			return false;
		if(text.charAt(0)=='-')
			text=text.substring(1,text.length());
		for (int i = 0; i < text.length(); i++) {
			if (!(text.charAt(i) >= '0' && text.charAt(i) <= '9')) {
				return false;
			}
		}
		if (Integer.parseInt(text) > 300)
			return false;
		return true;
	}

	public static void generarObjetivo(Datos juego) {
		if(generator.nextInt(2)==0)
			juego.setObjetivo(generator.nextInt(300)*(-1));
		else
			juego.setObjetivo(generator.nextInt(300));
	}

	public static void generarOperadores(Datos juego) {
		ArrayList<String> operadoresDisponibles=new ArrayList<String>();
		operadoresDisponibles.add("+");
		operadoresDisponibles.add("-");
		operadoresDisponibles.add("*");
		operadoresDisponibles.add("/");
		ArrayList<String> nuevosOperadores = new ArrayList<String>();
		nuevosOperadores.add(operadoresDisponibles.get(generator.nextInt(4)));
		nuevosOperadores.add(operadoresDisponibles.get(generator.nextInt(4)));
		juego.setOperadoresRestantes(nuevosOperadores);
	}

	public static void quitarOperador(Datos juego, String simbolo) {
		ArrayList<String> nuevosOperadores = new ArrayList<String>();
		nuevosOperadores = juego.getOperadoresRestantes();
		nuevosOperadores.remove(simbolo);
		juego.setOperadoresRestantes(nuevosOperadores);
	}

	public static void aumentarAciertos(Datos juego){
		juego.setAciertos(1);
	}
	
	public static void aumentarFallos(Datos juego){
		juego.setFallos(1);
	}
	
	public static int contarSignosMenos(String text){
		if (text.equals(null) || text.equals(""))
			return 0;
		int contador=0;
		for (int i = 0; i < text.length(); i++) {
			if(text.charAt(i)=='-'){
				contador++;
			}
		}return contador;
	}
	

}
